# Lab 9 - Exploratory Testing and UI Testing

Student name: <b>Marko Pezer</b><br>
Student email: <b>m.pezer@innopolis.university</b><br>
Student group: <b>BS18-SE-01</b>

## Website

Website for testing: [Russia Today](https://www.rt.com/)

## Implementation

Implementation of the Search test can be found in `src/test/java/TestSearch.java`.

## Test Tours

Document with Test Tours can be found here: [Google Docs](https://docs.google.com/document/d/1J4QgqtkYemwvrb8TcMOwiSfQMTXoXv1gFpMRRJW-Jrc/edit?usp=sharing)
