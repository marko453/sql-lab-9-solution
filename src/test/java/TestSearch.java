import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class TestSearch {

    WebDriver driver;
    private final String URL = "https://www.rt.com/";

    @BeforeClass
    public void setUp() {
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--headless");
        driver = new FirefoxDriver(options);
        driver.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
        driver.get(URL);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testAssetStore() {
        // Search
        Assert.assertEquals(driver.getTitle(), "RT - Breaking news, shows, podcasts");
        WebElement searchBox = driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div[2]/div[1]/div[2]/form/div/span/input"));
        searchBox.sendKeys("dusan tadic");
        searchBox.submit();
        // After Search
        Assert.assertEquals(driver.getTitle(), "Search results");
        WebElement publisherName = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[4]/div/section/div[1]/div[2]/div/div/div/ul/li[1]/div/div/div[2]/div[1]/a"));
        Assert.assertEquals(publisherName.getText(), "Fire rages at Netherlands national football stadium (VIDEO)");
    }
}
